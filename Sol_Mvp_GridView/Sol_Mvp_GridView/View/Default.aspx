﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Mvp_GridView.View.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
        table {
            width: 60%;
            margin: auto;
        }

         .auto-style1 {
             margin-left: 120px;
         }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSearch" runat="server" placeHolder="SearchSalesOrderId"></asp:TextBox>

                                 <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Label ID="lblSalesOrderNumber" Text="SalesOrderNumber" runat="server"></asp:Label>
                            </td>

                            <td>
                                <asp:TextBox ID="txtSalesOrderNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAccountNo" Text="AccountNo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAccountNo" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblOrderDate" Text="OrderDate" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrderDate" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCustomerAccountNo" Text="CustomerAccountNo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustomerAccountNo" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTerritoryName" Text="TerritoryName" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTerritoryName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCountryRegionCode" Text="CountryRegionCode" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCountryRegionCode" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCountryGroup" Text="CountryGroup" runat="server"></asp:Label>
                            </td>

                            <td>
                                <asp:TextBox ID="txtCountryGroup" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSubTotal" Text="SubTotal" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubTotal" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTaxAmt" Text="TaxAmt" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTaxAmt" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblFreight" Text="Freight" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFreight" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <asp:GridView ID="gvSalesOrderDetails" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyNames="SalesOrderId" GridLines="vertical" PageSize="5" ItemType="Sol_Mvp_GridView.Models.SalesOrderDetailsEntity" SelectMethod="gvSalesOrderDetails_GetData">
                                    <Columns>
                                         <asp:TemplateField HeaderText="Order Qty">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrderQty" runat="server" Text='<%#Item.OrderQty %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="UnitPrice">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrderQty" runat="server" Text='<%#Item.UnitPrice %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        Empty
                                    </EmptyDataTemplate>
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
