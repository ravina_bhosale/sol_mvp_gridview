﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Mvp_GridView.View
{
   public interface ISalesView
    {
         int? SalesOrderIdBinding { get; set; }

         String SearchBinding { get; set; }

         string OrderDateBinding { get; set; }

         String SalesOrderNumberBinding { get; set; }

         String AccountNoBinding { get; set; }

         String CustomerAccountNoBinding { get; set; }

         String TerritoryNameBinding { get; set; }

         String CountryRegionCodeBinding { get; set; }

         string CountryGroupBinding { get; set; }

         decimal? SubTotalBinding { get; set; }

         decimal? TaxAmtBinding { get; set; }

         decimal? FreightBinding { get; set; }
    }
}
