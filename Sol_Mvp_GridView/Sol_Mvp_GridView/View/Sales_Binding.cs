﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Mvp_GridView.View
{
    public partial class Default : ISalesView
    {
        public string AccountNoBinding
        {
            get
            {
                return txtAccountNo.Text;
            }
            set
            {
                txtAccountNo.Text = value;
            }
        }

        public string CountryGroupBinding
        {
            get
            {
                return txtCountryGroup.Text;
            }
            set
            {
                txtCountryGroup.Text = value;
            }
        }

        public string CountryRegionCodeBinding
        {
            get
            {
                return txtCountryRegionCode.Text;
            }
            set
            {
                txtCountryRegionCode.Text = value;
            }
        }

        public string CustomerAccountNoBinding
        {
            get
            {
                return txtCustomerAccountNo.Text;
            }
            set
            {
                txtCustomerAccountNo.Text = value;
            }
        }

        public decimal? FreightBinding
        {
            get
            {
                return Convert.ToDecimal(txtFreight.Text);
            }
            set
            {
                txtFreight.Text = value.ToString();
            }
        }

        public string OrderDateBinding
        {
            get
            {
                return txtOrderDate.Text;
            }
            set
            {
                txtOrderDate.Text = value;
            }
        }

        //public int? SalesOrderIdBinding
        //{
        //    get
        //    {
        //        return Convert.ToInt32(txt.Text);
        //    }
        //    set
        //    {
        //        txtSalesOrderNumber.Text = value.ToString();
        //    }
        //}

        public string SalesOrderNumberBinding
        {
            get
            {
                return txtSalesOrderNumber.Text;
            }
            set
            {
                txtSalesOrderNumber.Text = value;
            }
        }

        public string SearchBinding
        {
            get
            {
                return txtSearch.Text;
            }
            set
            {
                txtSearch.Text = value;
            }
        }

        public decimal? SubTotalBinding
        {
            get
            {
                return Convert.ToDecimal(txtSubTotal.Text);
            }
            set
            {
                txtSubTotal.Text = value.ToString();
            }
        }

        public decimal? TaxAmtBinding
        {
            get
            {
                return Convert.ToDecimal(txtTaxAmt.Text);
            }
            set
            {
                txtTaxAmt.Text = value.ToString();
            }
        }

        public string TerritoryNameBinding
        {
            get
            {
                return txtTerritoryName.Text;
            }
            set
            {
                txtTerritoryName.Text = value;
            }
        }
    }
}