﻿using Sol_Mvp_GridView.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Mvp_GridView.View
{
    public partial class Default : System.Web.UI.Page,ISalesView
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        //public IQueryable<Sol_Mvp_GridView.Models.SalesOrderDetailsEntity> gvSalesOrderDetails_GetData()
        //{
        //    return null;
        //}

        public async Task<SelectResult> gvSalesOrderDetails_GetData([Control("txtSearch")] string SerachSalesId, int startRowIndex, int maximumRows)
        {
            this.SalesOrderIdBinding = Convert.ToInt32(SerachSalesId);

            if (SalesOrderIdBinding != null)
            {
                // Create an Instance of defualtPresenter class
                var salesPresenterObj = new SalesPresenter(this);

                return await salesPresenterObj.BindSelesOrderDetailsData(startRowIndex, maximumRows, 
                            (leTotalCountSalesOrderDetailsObj, leListOfSalesOrderHeaderObj) => 
                            new SelectResult(Convert.ToInt32(leTotalCountSalesOrderDetailsObj), leListOfSalesOrderHeaderObj));
            }
            else
            {
                return null;
            }
        }

        #region IDefault View Implementation

        public int? SalesOrderIdBinding
        {
            get; set;
        }

        #endregion IDefault View Implementation

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            SalesPresenter salesPresenterObj = new SalesPresenter(this);
            await salesPresenterObj.SalesOrderHeaderDataBind();
        }
    }
}