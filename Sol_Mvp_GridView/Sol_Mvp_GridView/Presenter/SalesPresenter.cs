﻿using Sol_Mvp_GridView.DAL;
using Sol_Mvp_GridView.Models;
using Sol_Mvp_GridView.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_Mvp_GridView.Presenter
{
    public class SalesPresenter
    {
        #region Declaration

        private ISalesView _salesViewObj = null;

        #endregion Declaration

        #region Constructor

        public SalesPresenter(ISalesView salesView)
        {
            this._salesViewObj = salesView;
        }

        #endregion Constructor

        #region private Method
        private async Task SalesOrderHeaderDataMapping(SalesOrderHeaderEntity salesOrderHeaderObj)
        {
             await Task.Run(() =>
            {
                _salesViewObj.SalesOrderNumberBinding = salesOrderHeaderObj.SalesOrderNumber;
                _salesViewObj.OrderDateBinding = salesOrderHeaderObj.OrderDate;
                _salesViewObj.AccountNoBinding = salesOrderHeaderObj.AccountNo;
                _salesViewObj.CustomerAccountNoBinding = salesOrderHeaderObj.CustomerAccountNo;
                _salesViewObj.CountryRegionCodeBinding = salesOrderHeaderObj.CountryRegionCode;
                _salesViewObj.CountryGroupBinding = salesOrderHeaderObj.CountryGroup;
                _salesViewObj.SubTotalBinding = salesOrderHeaderObj.SubTotal;
                _salesViewObj.TaxAmtBinding = salesOrderHeaderObj.TaxAmt;
                _salesViewObj.FreightBinding = salesOrderHeaderObj.Freight;
                _salesViewObj.TerritoryNameBinding = salesOrderHeaderObj.TerritoryName;

            });
        }
        #endregion

        #region Public Method
        public async Task SalesOrderHeaderDataBind()
        {
            // Create an Instance of Sales Order Dal
            var saleDalObj = new SalesDal();

            //Get SalesOrderHeder Data
            var salesEntityObj = await saleDalObj.GetSalesOrderHeaderData(new SalesOrderHeaderEntity()
            {
                SalesOrderID = Convert.ToInt32(_salesViewObj.SearchBinding)
            });

            await SalesOrderHeaderDataMapping(salesEntityObj);
        }

        public async Task<SelectResult> BindSelesOrderDetailsData(int startRowIndex, int maximumRows, Func<int?, IEnumerable<SalesOrderDetailsEntity>, SelectResult> funcSelectMethod)
        {
            // Create an Instance of Sales Order Dal
            var salesDalObj = new SalesDal();

            // Get total Count of Sales Order Details
            var totalCount = await salesDalObj.GetTotalDetailsCount(Convert.ToInt32(_salesViewObj.SalesOrderIdBinding));

            // Get Sales Order Details Data
            var getSalesOrderDetailsData = await salesDalObj.SalesOrderDetailsDataPagination(Convert.ToInt32(_salesViewObj.SalesOrderIdBinding), startRowIndex, maximumRows);

            return funcSelectMethod(totalCount, getSalesOrderDetailsData);
        }
        #endregion
    }
}