﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Mvp_GridView.Models
{
    public class SalesOrderHeaderEntity
    {
        public int? SalesOrderID { get; set; }

        public String OrderDate { get; set; }

        public String SalesOrderNumber { get; set; }

        public String AccountNo { get; set; }

        public String CustomerAccountNo { get; set; }

        public String TerritoryName { get; set; }

        public String CountryRegionCode { get; set; }

        public String CountryGroup { get; set; }

        public decimal? SubTotal { get; set; }

        public decimal? TaxAmt { get; set; }

        public decimal? Freight { get; set; }

        public List<SalesOrderDetailsEntity> SalesOrderDetails { get; set; }
    }
}