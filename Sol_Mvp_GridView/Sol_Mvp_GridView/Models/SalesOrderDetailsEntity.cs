﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Mvp_GridView.Models
{
    public class SalesOrderDetailsEntity
    {
        public int? SalesOrderID { get; set; }

        public int? OrderQty { get; set; }

        public decimal? UnitPrice { get; set; }

    }
}