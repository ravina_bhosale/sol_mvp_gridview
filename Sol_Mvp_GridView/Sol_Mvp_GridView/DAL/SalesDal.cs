﻿using Sol_Mvp_GridView.DAL.ORD;
using Sol_Mvp_GridView.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_Mvp_GridView.DAL
{
    public class SalesDal
    {
        #region Declaration
        private SalesDcDataContext dc = null;
        #endregion

        #region constructor
        public SalesDal()
        {
            dc = new SalesDcDataContext();
        }
        #endregion
       
        #region Public method
        public async Task<SalesOrderHeaderEntity> GetSalesOrderHeaderData(SalesOrderHeaderEntity salesOrderHedaerEntityObj)
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {
                var getQuery=
                    dc
                    ?.uspGetSales(
                        "GetSalesOrederHederData",
                        salesOrderHedaerEntityObj.SalesOrderID,
                        ref status,
                        ref message
                        )
                    ?.AsEnumerable()
                    ?.Select((leSalesOrderHeaderObj) => new SalesOrderHeaderEntity()
                    {
                        SalesOrderID = leSalesOrderHeaderObj.SalesOrderID,
                        SalesOrderNumber = leSalesOrderHeaderObj.SalesOrderNumber,
                        AccountNo = leSalesOrderHeaderObj.AccountNumber,
                        CustomerAccountNo = leSalesOrderHeaderObj.AccountNumber1,
                        OrderDate = leSalesOrderHeaderObj.OrderDate.ToString(),
                        TerritoryName = leSalesOrderHeaderObj.Name,
                        CountryRegionCode = leSalesOrderHeaderObj.CountryRegionCode,
                        CountryGroup = leSalesOrderHeaderObj.Group,
                        SubTotal = leSalesOrderHeaderObj.SubTotal,
                        TaxAmt = leSalesOrderHeaderObj.TaxAmt,
                        Freight = leSalesOrderHeaderObj.Freight,
                        SalesOrderDetails = this.GetSalesOrderDetailsData(Convert.ToInt32(salesOrderHedaerEntityObj.SalesOrderID)).Result as List<SalesOrderDetailsEntity>

                    })
                    ?.FirstOrDefault();
                return getQuery;
            });
        }

        public async Task<IEnumerable<SalesOrderDetailsEntity>> GetSalesOrderDetailsData(int salesOrderId)
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {
                return
                    dc
                    ?.uspGetSales(
                        "GetSalesOrderDetailsData",
                        salesOrderId,
                        ref status,
                        ref message
                        )
                    ?.AsEnumerable()
                    ?.Where((leSalesOrderDetailsObj) => leSalesOrderDetailsObj.SalesOrderID == salesOrderId)
                    ?.Select((leSalesOrderDetailsObj) => new SalesOrderDetailsEntity()
                    {
                        SalesOrderID = leSalesOrderDetailsObj.SalesOrderID,
                        OrderQty = leSalesOrderDetailsObj.OrderQty,
                        UnitPrice = leSalesOrderDetailsObj.UnitPrice
                    })
                    
                    ?.ToList();
            });
        }

        public async Task<IEnumerable<SalesOrderDetailsEntity>> SalesOrderDetailsDataPagination(int salesOrderId, int startRowIndex, int maximumRows)
        {
           
                return await Task.Run(async () =>
                {
                    return
                       (await this.GetSalesOrderDetailsData(salesOrderId))
                        ?.AsEnumerable()
                        ?.Skip(startRowIndex)
                        ?.Take(maximumRows)
                        ?.ToList();
                });
        }

        public async Task<int?> GetTotalDetailsCount(int salesOrderId)
        { 

            return await Task.Run(async() =>
            {
                return
                   (await this.GetSalesOrderDetailsData(salesOrderId))
                    ?.AsEnumerable()
                    ?.Count((leSalesOrderDetailsObj) => leSalesOrderDetailsObj.SalesOrderID == salesOrderId);
            });
        }
        #endregion
    }
}