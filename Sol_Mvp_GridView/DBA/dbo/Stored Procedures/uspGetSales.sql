﻿CREATE PROCEDURE uspGetSales

	@Command Varchar(50)=NULL,

	@SalesOrderId int=NULL,

	@Status int= NULL OUT,
	@Message Varchar(MAX)=NULL OUT

AS
	
	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='GetSalesOrederHederData'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					SELECT 
						SOH.SalesOrderID,
						SOH.SalesOrderNumber,
						SOH.OrderDate,
						SOH.AccountNumber as 'HeaderAccountNo',
						SC.AccountNumber as 'CustomerAccountNo',
						ST.Name,
						ST.CountryRegionCode,
						ST.[Group],
						SOH.SubTotal,
						SOH.TaxAmt,
						SOH.Freight
						FROM Sales.SalesOrderHeader AS SOH
							LEFT JOIN
								Sales.Customer AS SC
									ON
										SOH.CustomerID=SC.CustomerID
											LEFT JOIN
												Sales.SalesTerritory AS ST
													ON
														SOH.TerritoryID=ST.TerritoryID
															WHERE SOH.SalesOrderID=@SalesOrderId

						SET @Status=1
						SET @Message='Select all sales header data'

						COMMIT TRANSACTION
							
				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

				END
				ELSE IF @Command='GetSalesOrderDetailsData'
					BEGIN
				
					BEGIN TRANSACTION

					BEGIN TRY
						SELECT 
							SOD.SalesOrderID,
							SOD.OrderQty,
							SOD.UnitPrice
								FROM Sales.SalesOrderDetail AS SOD
									WHERE SOD.SalesOrderID=@SalesOrderId

						SET @Status=1
						SET @Message='Select all sales header data'

						COMMIT TRANSACTION
					END TRY

					BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 
			END
		END
